all: add_nbo

add_nbo: main.o add_nbo.o
	gcc -o add_nbo main.o add_nbo.o

main.o: main.c add_nbo.h
	gcc -c -o main.o main.c

add_nbo.o: add_nbo.c add_nbo.h
	gcc -c -o add_nbo.o add_nbo.c

clean: 
	rm -rf *.o
	rm -rf add_nbo