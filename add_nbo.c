#include "add_nbo.h"

uint32_t change_num(uint32_t num)
{
    uint32_t val;

    val = ((num & 0xff) << 24 | (num & 0xff00) << 8 | 
            (num & 0xff0000) >> 8 | (num & 0xff000000) >> 24);

    return (val);
}

void add_nbo(uint32_t num_f, uint32_t num_s)
{
    uint32_t num1, num2, sum;

    num1 = change_num(num_f);
    num2 = change_num(num_s);
    sum = num1 + num2;
    
    printf("%d(0x%x) + %d(0x%x) = %d(0x%x)\n", num1, num1, num2, num2, sum, sum);
}