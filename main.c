#include "add_nbo.h"

int main(int argc, char *argv[])
{
    FILE *num1 = fopen(argv[1], "rb");
    FILE *num2 = fopen(argv[2], "rb");

    uint32_t num_f, num_s;

    if (num1 && num2)
    {
        fread(&num_f, sizeof(uint32_t), 1, num1);
        fread(&num_s, sizeof(uint32_t), 1, num2);
    }

    add_nbo(num_f, num_s);

    fclose(num1);
    fclose(num2);

    return (0);
}